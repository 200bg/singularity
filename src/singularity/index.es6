// math
const TAU = Math.PI * 2;
const PI = TAU / 2;
const Q = TAU / 4;


class Singularity {
    constructor(canvas, capture=false) {
        this.canvas = canvas;
        this.debug = false;
        this.currentFrame = 0;
        this.duration = 0;

        this.tickProxy = this.tick.bind(this);

        this.capture = capture;
        // set this later
        this.maxCaptures = 0;
        if (this.capture) {
            this.captures = [];
        }
    }

    start() {
        this.ctx = this.canvas.getContext('2d');
        this.tickId = requestAnimationFrame(this.tickProxy);
    }

    stop() {
        cancelAnimationFrame(this.tickId);
    }

    tick(t) {
        var i;
        // all the shapes are a slave to this beat
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.save();
        this.draw(t);
        this.tickId = requestAnimationFrame(this.tickProxy);
    }

    draw(t) {
        // draw, iterate, set captures here.
    }

    createCapture() {
        var dataUrl = this.canvas.toDataURL();
        this.captures.push(dataUrl);
    }

    saveNextCapture(fs, i) {
        var blob, capture = this.captures[i];
        if (!capture) return;
        blob = captureToBlob(capture.replace(/^[^,]+,/, ''));
        fs.root.getFile('capture-' + i + '.png', {create: true}, (function(fileEntry) { // test.bin is filename
            fileEntry.createWriter((function(fileWriter) {

                fileWriter.addEventListener("writeend", (function() {
                    console.log('Wrote capture ' + i + '; capture-' + i + '.png');
                    if (i < this.captures.length) {
                        i = i + 1;
                        this.saveNextCapture(fs, i);
                    }
                }).bind(this), false);

                fileWriter.write(blob);
            }).bind(this), function() {});
        }).bind(this), function() {});
    }

    saveCaptures(fileSystem=true) {
        var captures = this.captures;
        if (fileSystem) {
            window.requestFileSystem(window.TEMPORARY, 5*1024*1024, (function(fs) {
                this.saveNextCapture(fs, 0);
            }).bind(this), function() {});
        } else {
            // post to the /captures/ url.
            var i, url, capture, xhr;
            url = 'http://localhost:8001/capture/';

            for (i = 0; i < captures.length; i++) {
                capture = captures[i];
                if (capture) {
                    xhr = new XMLHttpRequest();
                    xhr.open('POST', url + 'capture-' + i + '.png', true);
                    xhr.send(capture.replace(/^[^,]+,/, ''));
                }
            };
        }
    }
}

function captureToBlob(base64String) {
    var chunkSize, base64Data, byteArrays, byteArray, offset, chunk, byteInts, i, blob;
    chunkSize = 512;
    offset = 0;
    base64Data = atob(base64String);
    byteArrays = [];

    for (offset = 0; offset < base64Data.length; offset += chunkSize) {
        chunk = base64Data.slice(offset, offset + chunkSize);

        byteInts = new Array(chunk.length);
        for (i = 0; i < chunk.length; i++) {
            byteInts[i] = chunk.charCodeAt(i);
        }

        byteArray = new Uint8Array(byteInts);

        byteArrays.push(byteArray);
    }

    blob = new Blob(byteArrays);
    return blob;
}

function SaveCapturesMiddleWare(req, res, next) {
    var fs = require('fs');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,HEAD,OPTIONS');
    res.setHeader('Allow', 'GET, POST, HEAD, OPTIONS');
    if (req.url.indexOf('/capture/') == 0 && req.method == 'POST') {
        req.setEncoding('utf-8');
        req.on('data', function (rawData) {
            var filename = req.url.substr(9);
            var data = rawData;
            console.log('saving ./saves/' + filename);
            fs.writeFile('./saves/' + filename, rawData, 'base64', function(err) {
                if (err) {
                    console.log(err);
                    res.end('FAIL');
                } else {
                    res.end('OK');
                }
            });
        });
    } else {
        return next();
    }
}

export {
    Singularity,
    captureToBlob,
    SaveCapturesMiddleWare,
    TAU,
    Q
}
