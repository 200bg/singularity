var fs = require('fs');
var singularity = require('./lib/singularity/index.js');


module.exports = function (grunt) {
    grunt.initConfig({
        'babel': {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'lib/singularity/index.js': 'src/singularity/index.es6'
                }
            }
        },
        connect: {
            server: {
                options: {
                    port: 8001,
                    useAvailablePort: true,
                    base: '.',
                    keepalive: false,
                    middleware: function(connect, options, middlewares) {
                        middlewares.unshift(singularity.SaveCapturesMiddleWare);
                        return middlewares;
                    },
                }
            },
        },
        'browserify': {
            options: {
                alias: [
                    './lib/singularity/index.js:singularity',
                ],
                browserifyOptions: {}
            }, 
            dist: {
                files: {
                    './dist/singularity.js': 'singularity',
                }
            }
        },
        watch: {
          scripts: {
            files: ['src/**/*.es6'],
            tasks: ['babel', 'browserify'],
            options: {
              spawn: false,
            },
          },
        },
    });

    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default', ['babel', 'browserify']);
    grunt.registerTask('server', ['connect', 'watch']);
};
